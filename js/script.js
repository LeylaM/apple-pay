/* Burger Menu */

let headerBurger = document.getElementById("header-burger");
let mobileNav = document.getElementById("mobile-nav");
let body = document.body;
let mobileNavClasses = mobileNav.classList;
let menuIcon = document.getElementById("menu-icon");
let headerSticky = document.getElementById('header-sticky');


function openMobileNav(){
    body.classList.add("of-hidden");
    menuIcon.innerHTML='<use xlink:href="img/sprite.svg#Close"></use>';
    mobileNavClasses.add('visible');
    headerSticky.classList.add('header-sticky-mobile');
}

function closeMobileNav(){
    body.classList.remove("of-hidden")
    menuIcon.innerHTML='<use xlink:href="img/sprite.svg#Menu"></use>';
    mobileNavClasses.remove('visible');
    headerSticky.classList.remove('header-sticky-mobile');
}

headerBurger.addEventListener('click', () => {
    if (!mobileNavClasses.contains('visible')) {
        openMobileNav();

    } else {
        closeMobileNav();
    }
});

[...mobileNav.children].forEach(item => {
    item.addEventListener('click', closeMobileNav)
})


/*Header Nav Items Hover*/

let navItems = document.querySelectorAll('#header-nav .nav-item');

navItems.forEach((item) => {
    item.addEventListener("mouseover", (event) => {
        navItems.forEach((navItem) => navItem.classList.add('nav-item-inactive'))
        event.target.classList.remove("nav-item-inactive");
    })

    item.addEventListener('mouseout',() => {
        navItems.forEach((navItem) => navItem.classList.remove("nav-item-inactive"))
    })
})

/*Tabs*/

function changeTabs(tabContainerId, textContentContainerId, imgContentContainerId){

    let tabs = [...document.getElementById(tabContainerId).children];
    let textContent = [...document.getElementById(textContentContainerId).children];
    let imgContent = [...document.getElementById(imgContentContainerId).children];

    tabs.forEach( tab => {
        tab.addEventListener('click', (event) => {

            let activeTabTextContent = document.querySelector(tab.dataset.tabTextContent);
            let activeTabImgContent = document.querySelector(tab.dataset.tabImgContent);


            tabs.forEach( tab => {
                tab.classList.remove('how-to-btn-active');
            });

            textContent.forEach( text => {
                text.classList.add('hidden');
            });

            imgContent.forEach( img => {
                img.classList.add('hidden');
            })

            event.target.classList.add('how-to-btn-active');
            activeTabTextContent.classList.remove('hidden');
            activeTabImgContent.classList.remove('hidden');
        })
    })
}

changeTabs('tab-container1', 'tab-text-content1', 'tab-img-content1');
changeTabs('tab-container2', 'tab-text-content2', 'tab-img-content2');

/*Modal*/

let iframes = [...document.querySelectorAll('[data-modal-iframe]')];
let imgContent = [...document.getElementById('tab-img-content2').children];
let modal = document.querySelector('#modal');
let modalBtn=document.getElementById('modal-btn');

imgContent.forEach((item) => {
    item.addEventListener("click", () => {

        modal.classList.add('modal-active');

        let clickedItem = document.querySelector(item.dataset.modalVideo);

        iframes.forEach( iframe => {
            iframe.classList.add('hidden');
        });

        clickedItem.classList.remove('hidden');

    })
})

function closeModal () {
    let modal = document.querySelector('#modal');
    modal.classList.remove('modal-active');
}

function windowOnClick(event) {
    if (event.target === modal) {
        closeModal();
    }
}

modalBtn.addEventListener('click', closeModal);
window.addEventListener("click", windowOnClick);



document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();
        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});