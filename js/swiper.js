const swiper = new Swiper('.swiper', {
    // Optional parameters
    direction: 'horizontal',
    breakpoints: {
      // when window width is >= 320px
      320: {
        slidesPerView: 1.2,
        spaceBetween: 16
      },
      // when window width is >= 767px
      767: {
        slidesPerView: 2.2,
        spaceBetween: 24
      },
      // when window width is >= 1034px
      1034: {
        slidesPerView: 3,
        spaceBetween: 30
        
      }
    }
  });